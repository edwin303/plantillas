create database BDPlantilla

go

create login adminplatilla with password = 'sistemas123'

go

use BDPlantilla

go

-- Agregando usuario
sp_adduser adminplatilla, adminplatilla

go

-- Dando permiso de seleccion
create table Cliente (
id int
)

go
grant select on Cliente to adminplatilla

-- revocando permiso de seleccion
go
revoke select on Cliente to adminplatilla

-- Todo lo anterior era con proposito de coneccion


go
--Tabla para el registro de usuarios
Create table Usuario (
IdUsuario int primary key identity(1,1),
usuario varchar(80),
contraseņa varchar(80),
rol varchar(80),
estado varchar(80))

go
 -- Crear tabla colaborador

 create table Colaborador
 (
 IdColaborador int primary key identity(1,1),
 primernombre varchar(60),
 primerapellido varchar(60)
 )

 go

 insert into Colaborador values ('Ana','Mendoza')
 go
 select  * from Colaborador

 go

 alter table Usuario
 add IdColaborador int

 go

 select * from Usuario
 go

 update Usuario set IdColaborador = 1 where IdUsuario = 2

--- Procedimiento almacenado para insertar el usuario
go

Create procedure [dbo].[Insertar_Usuario]
 @usuario varchar(50), @contraseņa varchar(50), @rol varchar(50)
 as
 insert into Usuario(usuario, contraseņa, rol, Estado) values
 (@usuario, ENCRYPTBYPASSPHRASE( @contraseņa,  @contraseņa), @rol, 'Habilitado')

 -- Probando Insert con respecto al procedimiento de almacenado
 /*
 insert into Usuario(usuario, contraseņa, rol)
 values ('uni', 'sistemas2021', 'Administrador')*/

 --Ejecucion de procedimiento de almacenado
/*
 Execute dbo.Insertar_Usuario 'uni', 'sistemas2021', 'Administrador'
 Select * from Usuario
 update Usuario set estado = 'Habilitado' where IdUsuario = 1
 */

 ----------------------------------------
 go

 -- Algoritmo para validar acceso a la base de datos en el login
alter procedure [dbo].[Validar_Acceso]
@usuario varchar(50),
@contraseņa varchar(50)
as
if exists (Select usuario from Usuario
            where  cast (DECRYPTBYPASSPHRASE(@contraseņa, contraseņa) as Varchar(100)) = @contraseņa
			 and usuario = @Usuario and Estado = 'Habilitado' )
			
				Select 'Acceso Exitoso'  as Resultado, c.primernombre + ' ' + c.primerapellido, rol
				from Usuario u inner join Colaborador c
				on c.IdColaborador = u.idColaborador
				where  cast (DECRYPTBYPASSPHRASE(@contraseņa, u.contraseņa) as Varchar(100)) = @contraseņa
			 and u.usuario = @Usuario and u.Estado = 'Habilitado'

			 else
				Select 'Acceso Denegado' as Resultado

-- Probando stored procedure
 -- Execute dbo.Validar_Acceso 'uni', 'sistemas2021'

 -- Select @@SERVERNAME

 go

 --Perminir que usuario adminplatilla pueda ejecutar el procedimiento de almacenado
 Grant execute on Validar_Acceso to adminplatilla
