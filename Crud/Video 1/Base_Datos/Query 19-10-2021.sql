
Create database GranHotel
go
Create login adminGranHotel with password = 'sistemas123'
go
Use GranHotel
go
sp_adduser adminGranHotel, adminGranHotel
Create table Cliente(id int)
Revoke select on Cliente to adminGranHotel
Grant execute on Validar_Acceso to adminGranHotel

Create table Usuario (IdUsuario int primary key identity(1,1),
usuario varchar(80),
contraseņa varchar(80),
rol varchar(80),
estado varchar(80))

--- Procedimiento almacenado para insertar el usuario

Create procedure [dbo].[Insertar_Usuario]
 @usuario varchar(50), @contraseņa varchar(50), @rol varchar(50)
 as
 insert into Usuario(usuario, contraseņa, rol, Estado) values
 (@usuario, ENCRYPTBYPASSPHRASE( @contraseņa,  @contraseņa), @rol, 'Habilitado')

 insert into Usuario(usuario, contraseņa, rol, estado)
 values ('uni', 'sistemas2021', 'Administrador')

 Execute dbo.Insertar_Usuario 'uni', 'sistemas2021', 'Administrador'
 Select * from Usuario
 update Usuario set estado = 'Habilitado' where IdUsuario = 1

 ----------------------------------------
Alter procedure [dbo].[Validar_Acceso]
@usuario varchar(50),
@contraseņa varchar(50)
as
if exists (Select usuario from Usuario
            where  cast (DECRYPTBYPASSPHRASE(@contraseņa, contraseņa) as Varchar(100)) = @contraseņa
			 and usuario = @Usuario and Estado = 'Habilitado' )
			 select 'Acceso Exitoso' as Resultado, c.primernombre +' '+c.primerapellido, rol
			 from Usuario u
			 inner join Colaborador c
			 on c.IdColaborador = u.IdColaborador
			  where  cast (DECRYPTBYPASSPHRASE(@contraseņa, u.contraseņa) as Varchar(100)) = @contraseņa
			 and u.usuario = @Usuario and u.Estado = 'Habilitado'

			 else
			 Select 'Acceso Denegado' as Resultado


			 Execute dbo.Validar_Acceso 'uni', 'sistemas2021'

			 Select @@SERVERNAME

Create table Colaborador
(IdColaborador int primary key identity(1,1),
 primernombre varchar(60),
 primerapellido varchar(60))

 insert into Colaborador values ('Ana', 'Mendoza')
 Select * from Colaborador
 Alter table Usuario
 add IdColaborador int

 Select * from Usuario
 update Usuario set IdColaborador = 1