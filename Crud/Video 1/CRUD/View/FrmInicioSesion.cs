﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Bibliotecas Implementadas:
using CRUD.Controller;

namespace CRUD.View
{
    public partial class FrmInicioSesion : Form
    {
        public FrmInicioSesion()
        {
            InitializeComponent();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            DataTable dato;
            string nombreUsuario;
            string rol;
            dato = CUsuario.Validar_Acceso(this.txtUsuario.Text, this.txtContraseña.Text);

            if (dato != null)
            {

                if (dato.Rows.Count > 0)
                {
                    /*DataRow dr;
                    dr = dato.Rows[0];*/

                    if (dato.Rows[0][0].ToString() == "Acceso Exitoso")
                    {
                        nombreUsuario = dato.Rows[0][1].ToString();
                        rol = dato.Rows[0][2].ToString();
                        MessageBox.Show($"Bienvenido al Sistema", "Plantilla", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Form1 fc = new Form1(nombreUsuario, rol);
                        fc.Show();
                        //fc.FormClosed += logout;
                        this.Hide();

                    }
                    else
                    {
                        MessageBox.Show("Acceso Denegado al Sistema de Reservaciones", "Plantilla", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        /*this.txtUsuario.Text = string.Empty;
                        this.txtContraseña.Text = string.Empty;
                        this.txtUsuario.Focus();*/
                    }
                }
            }
            else
                MessageBox.Show("No hay conexión al servidor", "Plantilla", MessageBoxButtons.OK, MessageBoxIcon.Error);

        }
    }
}
