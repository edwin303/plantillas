﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CRUD
{
    public partial class Form1 : Form
    {
        string nombreUsuario;
        string rol;
        public Form1(string nombreUsuario, string rol)
        {
            InitializeComponent();
            nombreUsuario = nombreUsuario;
            rol = rol;
        }

        private void Form1_Load(Object sender, EventArgs e)
        {
            this.label1.Text = nombreUsuario;
        }
    }
}
